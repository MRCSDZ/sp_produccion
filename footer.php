<!-- ../Footer -->
<footer class="footer">
    <div class="container text-justify upper-footer">
        <div class="row" style="padding-top: 20px;">
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <h3 class="footer-title">Contactanos</h3>
                <ul>
                    <li><a href="http://sp_chat.pentafon.com/" class="text-muted"><i class="hidden-xs glyphicon glyphicon-comment" aria-hidden="true"></i> Chatea con nosotros!</a></li>
                    <li><a href="http://sp_webcallback.pentafon.com//" class="text-muted"><i class="hidden-xs glyphicon glyphicon-phone-alt" aria-hidden="true"></i> Nosotros te llamamos!</a></li>
                    <li><a href="http://seguro-popular.gob.mx/Ayuda/index.php?a=add" class="text-muted"><i class="hidden-xs glyphicon glyphicon-inbox" aria-hidden="true"></i> Queremos tu opinion!</a></li>
                    <!-- <li><a class="text-muted"><i class="hidden-xs glyphicon glyphicon-earphone" aria-hidden="true"></i> 01 800 767 85 27 </a></li> -->
                </ul>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <h3 class="footer-title">Secciones</h3>
                <ul>
                    <li><a href="/" class="text-muted"><i class="hidden-xs glyphicon glyphicon-home"></i>Inicio</a></li>
                    <li><a href="#" class="text-muted"><i class="hidden-xs glyphicon glyphicon-book"></i>Diccionario</a></li>
                    <li><a href="FAQ.php" class="text-muted"><i class="hidden-xs glyphicon glyphicon-question-sign"></i>Preguntas Frecuentes</a></li>
                    <li><a href="public/pdfs/Anexos2016.pdf" class="text-muted" target="_blank"><i class="hidden-xs glyphicon glyphicon-file"></i>Anexos</a></li>
                    <li><a href="public/pdfs/AvisoPrivacidad.pdf" target="_blank" class="text-muted"><i class="hidden-xs glyphicon glyphicon-lock"></i>Aviso de privacidad</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <h3 class="footer-title">DIRECTORIO</h3>
                <ul>
                    <li><a href="/" class="text-muted"><i class="hidden-xs glyphicon glyphicon-retweet"></i>Intranet</a></li>
                    <li><a href="#" class="text-muted"><i class="hidden-xs glyphicon glyphicon-folder-open"></i>Archivo</a></li>
                    <li><a href="FAQ.html" class="text-muted"><i class="hidden-xs glyphicon glyphicon-question-sign"></i>SINOS</a></li>
                    <li><a href="public/pdfs/Anexos2016.pdf" class="text-muted" target="_blank"><i class="hidden-xs glyphicon glyphicon-globe"></i>Email</a></li>
                    <li><a href="http://www.transparenciabc.gob.mx/" target="_blank" class="text-muted"><i class="hidden-xs glyphicon glyphicon-search"></i>Transparencia</a></li>

                </ul>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <h3 class="footer-title">Siguenos</h3>
                <ul>
                    <li><a href="https://www.facebook.com/seguropopularbc/" target="_blank" class="text-muted"><img src="public/images/facebook.png" width="25" height="25" alt=""  style="margin-right: 15px;"></i>Facebook</a></li>
                    <li style="margin-right: 15px;"><a href="https://twitter.com/BCSeguroPopular" target="_blank" class="text-muted"><img src="public/images/twitter.png" width="25" height="25" alt="" style="margin-right: 15px;">Twitter</a></li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="container-fluid lower-footer">
        <div class="row text-center">
            <div class="col-lg-12">
                <p class="text-muted">© RÉGIMEN DE PROTECCIÓN SOCIAL DE SALUD, DEL ESTADO DE BAJA CALIFORNIA. MEXICO 2017</p>
            </div>
        </div>
        <div class="row text-center" >
            <ul style="text-transform: uppercase;">
                <li style="display: inline-block;padding: 3px;"><a href="http://sap.seguropopularbc.gob.mx/sap/" class="text-muted">SAP |</a></li>
                <li style="display: inline-block;padding: 3px;"><a href="http://www.sinapticait.net/" class="text-muted"> Intranet |</a></li>
                <li style="display: inline-block;padding: 3px;"><a href="http://www.sinapticait.net/login.asp?guia=/ArchDigital/default.asp" class="text-muted"> Archivo | </a></li>
                <li style="display: inline-block;padding: 3px;"><a href="https://mail.seguropopularbc.gob.mx/webmail/login/" class="text-muted"> email</a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- ../Footer -->
