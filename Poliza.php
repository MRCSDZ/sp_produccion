<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->


		<section id="">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title">Consultar Póliza</h3>
						<br>
              <div class="col-lg-6">
								<div class="form-group">
									<h3>Consulta por Nombre</h3>
									<label for="">Nombre(s)*</label>
									<input class="form-control" type="text" name="" value="" required>
									<label for="">Apellido Paterno*</label>
									<input class="form-control" type="text" name="" value="" required>
									<label for="">Apellido Materno*</label>
									<input class="form-control" type="text" name="" value="" required>
									<small class="form-text text-muted">Los datos con * son requeridos.</small>
									<br>
									<br>
									<button type="button" class="btn btn-default ">
  									<span class="glyphicon glyphicon-search" ></span> Buscar
									</button>

								</div>

              </div>
              <div class="col-lg-6">
								<h3>Consulta por CURP y Folio</h3>
								<div class="form-group">
									<label for="">Folio*</label>
									<input class="form-control" type="text" name="" value="" required>
									<label for="">CURP*</label>
									<input class="form-control" type="text" name="" value="" required>
									<small class="form-text text-muted">Los datos con * son requeridos.</small>
									<br>
									<br>
									<button type="button" class="btn btn-default ">
  									<span class="glyphicon glyphicon-search" ></span> Buscar
									</button>
								</div>



              </div>

					</div>
				</div>
			</div>
		</section>

		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	    <!-- <script type="text/javascript" src="public/js/owl.carousel.min"></script> -->

	</body>
</html>
