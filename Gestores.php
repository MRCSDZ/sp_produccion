<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->


		<section id="">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="text-center">¿Quién es el Gestor del Seguro Popular?</h3>
						<center>
							<img src="public/images/gestores_tijuana.jpg" alt="" width="70%"/>
						</center>
						<br>
						<br>
						<p class="text-justify">Yo soy tu Gestor del Seguro Popular y me puedes encontrar en tu hospital o centro de salud. Estoy para asesorarte y resolver tus dudas con respecto a los servicios de salud que puedes recibir como beneficiario del Sistema de Protección Social en Salud y guiarte en el proceso de atención médica para que se te otorgue un servicio eficiente, de calidad y sin costo alguno.
						</p>
					</div>
				</div>
			</div>
		</section>

		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	    <!-- <script type="text/javascript" src="public/js/owl.carousel.min"></script> -->

	</body>
</html>
