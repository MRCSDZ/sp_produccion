# Pagina Seguro Popular de Baja California #
#
Este repositorio contiene la pagina del Seguro Popular de Baja california como se muestra en la pagina de [www.seguropopular.info](http://www.seguropopular.info/)
#
#
##Realizado con##
* PHP
* HTML5
* CSS3
* Bootstrap
* JQuery
## Requerimientos de Instalación ##
* Servidor con PHP +5.0
* MySQL 
* phpMyAdmin

##Autores##
* **Rafael Rios**
* **Marcos Diaz**

### Nota: ###
El apartado de sala de prensa es una instalación aparte usando el CMS **Wordpress** el cual no viene incluido en este repositorio pero se puede visualizar dentro de la pagina presentada anteriormente.
#
#


![screensp.jpg](https://bitbucket.org/repo/GgnL94L/images/3248966926-screensp.jpg)