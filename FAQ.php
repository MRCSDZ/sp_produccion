<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->

		<section id="FAQ">
			<div class="container">
				<div class="row">
					<h3 class="title">Preguntas Frecuentes</h3>
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Sección 1</div>
							<div class="panel-body">
								<ul class="nav nav-tabs">
								    <li class="active"><a data-toggle="tab" href="#Uno">Breve Historia</a></li>
								    <li><a data-toggle="tab" href="#Dos">¿Que es?</a></li>
								    <li><a data-toggle="tab" href="#Tres">¿Para quién es?</a></li>
								    <li><a data-toggle="tab" href="#Cuatro">¿Es Gratuito?</a></li>
									<li><a data-toggle="tab" href="#Cinco">¿Cuando inicia la Vigencia?</a></li>
									<li><a data-toggle="tab" href="#Seis">¿Puedo Afiliarme si Padezco una Enfermedad?</a></li>
								</ul>
								<div class="tab-content">
								    <div id="Uno" class="tab-pane fade in active">
										<p align="justify">Por primera vez en la historia mexicana, existe un seguro médico, público y voluntario, dirigido a poblaciones sin acceso a servicios de salud. En su fase piloto el Seguro Popular fue capaz de afiliar a más de un millón de personas entre 2001 y 2003. La necesidad de una reforma financiera en las instituciones públicas de salud se hizo patente y en el 2001 se dieron los primeros pasos para diseñar e implantar políticas de largo plazo. La fase piloto del Seguro Popular duró tres años (2001-2003) y alcanzó importantes avances en su operación. El programa definió inicialmente un paquete de 78 intervenciones tanto de primer como de segundo nivel de atención. Estas intervenciones cubren el 85% del total de la demanda de atención.	Después de un proceso de negociaciones entre la Secretaría de Salud, el	Congreso y los Gobiernos de los Estados, en febrero de 2005 se había logrado incorporar ya a 31 estados. Cada estado tiene la posibilidad de agregar intervenciones al paquete de acuerdo a su conveniencia y capacidad financiera.</p>
								    </div>
								    <div id="Dos" class="tab-pane fade">
										<p align="justify">El Seguro Popular es un seguro médico, público y voluntario, que fomenta la atención oportuna a la salud, a través de un mecanismo de protección del patrimonio familiar. Además, ofrece atención médica, estudios y medicamentos sin costo al momento de utilizarlos. Garantiza el acceso a un paquete de servicios de salud, que cubre 287 padecimientos, y a los medicamentos asociados a dichos padecimientos.</p>
								    </div>
								    <div id="Tres" class="tab-pane fade">
										<p align="justify">Para las familias y personas que no sean derechohabientes de las instituciones de seguridad social o no cuenten con algún otro mecanismo de previsión social en salud.</p>
								    </div>
								    <div id="Cuatro" class="tab-pane fade">
										<p align="justify">
											En la mayoría de los casos es gratuito, pero para determinar la capacidad de pago de una familia se realiza una evaluación socioeconómica utilizando la Cédula de Características Socioeconómicas del Hogar.
										</p>
								    </div>
									<div id="Cinco" class="tab-pane fade">
										<p align="justify">
											El inicio de la vigencia de los derechos comienza día y hora despues de su incorporación, y puede ser alguna de las siguientes:
											Vigencia de 90 días: familias con expediente incompleto.
											Vigencia de 3 años: familias con expediente completo.
										</p>
									</div>
									<div id="Seis" class="tab-pane fade">
										<p align="justify">
											 Si, en el Seguro Popular puedes solicitar tú afiliación estés sano o enfermo. Sin embargo, lo idóneo es solicitar tu afiliación cuando estés sano, a fin de fomentar la cultura de la prevención y la atención oportuna de la salud.
										</p>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">Seccion 2</div>
							<div class="panel-body">
								<ul class="nav nav-tabs">
								    <li class="active"><a data-toggle="tab" href="#DosUno">No tengo todos los documentos ¿Puedo Afiliarme?</a></li>
								    <li><a data-toggle="tab" href="#DosDos">Perdi la poliza</a></li>
								    <li><a data-toggle="tab" href="#DosTres">No tengo IFE</a></li>
								    <li><a data-toggle="tab" href="#DosCuatro">¿Tengo Cobertura Fuera del Estado?</a></li>
									<li><a data-toggle="tab" href="#DosCinco">¿Donde Consulto mi CURP</a></li>
								</ul>

				  				<div class="tab-content">
				    				<div id="DosUno" class="tab-pane fade in active">
										<p align="justify">Si, cuentas con un periodo de 90 días naturales para completar la entrega de documentos, de lo contrario se dará por no presentada la solicitud de incorporación.</p>
				    				</div>
				    				<div id="DosDos" class="tab-pane fade">
										<p align="justify">La CURP la puedes consultar en: <a href="http://consultas.curp.gob.mx/CurpSP/">http://consultas.curp.gob.mx/CurpSP/</a></p>
				    				</div>
				    				<div id="DosTres" class="tab-pane fade">
										<p align="justify">Puedes presentar pasaporte, licencia de conducir o cualquier identificación oficial con fotografía emitida por autoridad competente. En caso de que no cuentes con identificación oficial, podrás presentar Carta de autoridad local como medio de identificación, en tanto se logra un registro oficial. Así mismo, tratándose de localidades catalogadas como de alta y muy alta marginación que cuenten con menos de 250 habitantes, se podrá entregar copia del Acta de Asamblea Ejidal o Comunal o Acta levantada ante dos testigos.</p>
				    				</div>
				    				<div id="DosCuatro" class="tab-pane fade">
										<p align="justify"> Si, siempre y cuando presentes tu póliza y te encuentres en alguna de las siguientes circunstancias:</p>
										<li>Cuando el paciente sea referido por el Estado de origen</li>
										<li>En caso de urgencia médica.</li>
										<li>Por cercanía geográfica.</li>
										<li>En caso de pacientes en tránsito.</li>
								    </div>
									<div id="DosCinco" class="tab-pane fade">
										<p align="justify">Acude al Módulo de Afiliación en el que te afiliaste para solicitar una copia de tu póliza.</p>
									</div>
							  	</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">Seccion 3</div>
							<div class="panel-body">
								<ul class="nav nav-tabs">
								    <li class="active"><a data-toggle="tab" href="#TresUno">¿Qué Padecimientos Cubre?</a></li>
								    <li><a data-toggle="tab" href="#TresDos">¿Dónde me Pueden Atender?</a></li>
								</ul>
						  		<div class="tab-content">
								    <div id="TresUno" class="tab-pane fade in active">
										<p align="justify">Actualmente el Seguro Popular cubre 287 intervenciones médicas contenidas en el Catálogo Universal de Servicios de Salud <a href="docs/pdf/ANEXO1_2016.pdf"> Causes 2016</a> que cubren más del 90 por ciento de acciones en hospitales y 100 por ciento en centros de salud, así como intervenciones correspondientes al Fondo de Protección contra Gastos Catastróficos y Seguro Médico Siglo XXI.</p>
								    </div>
								    <div id="TresDos" class="tab-pane fade">
										<p align="justify">La prestación de servicios de salud a los beneficiarios se efectua de forma directa en las unidades médicas de la Red de Prestadores de Servicios, la cual está integrada principalmente por centros de salud y hospitales de los Servicios Estatales de Salud.</p>
								    </div>
							  	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->


	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	</body>
</html>
