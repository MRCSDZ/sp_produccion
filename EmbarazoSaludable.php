<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->


		<section id="">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title">Embarazo Saludable</h3>
						<br>
              <div class="col-lg-12">
                <img src="public/images/embarazada.jpg" width="40%" alt="" class="center-block">
                <br>
                <br>
              </div>
              <div class="col-lg-12">
                <p>
                  Esta estrategia forma parte del Sistema de Protección Social en Salud y fue creada como un complemento al Programa Seguro
                  Médico Siglo XXI, tiene como propósito proteger la salud de la mujer durante el embarazo, al igual que la del recién nacido.
                </p>
                <p>
                  <b>Las mujeres beneficiarias de esta estrategia tendrán acceso a los siguientes servicios:</b>
                  <li>Atención prenatal en el embarazo</li>
                  <li>Atención de parto y puerperio fisiológico</li>
                  <li>Atención de cesárea y puerperio quirúrgico</li>
                </p>
                <p>
                  <b>En particular tienen derecho a:</b>

                  <li>Atención quirúrgica de la enfermedad trofoblástica</li>
                  <li>Tratamiento quirúrgico de embarazo ectópico</li>
                  <li>Legrado uterino terapéutico por aborto incompleto</li>
                  <li>Diagnóstico y tratamiento de pre-eclampsia</li>
                  <li>Diagnóstico y tratamiento de pre-eclampsia severa</li>
                  <li>Diagnóstico y tratamiento de eclampsia</li>
                  <li>Diagnóstico y tratamiento de amenaza de aborto y parto pretérmino</li>
                  <li>Infección de episiorrafía o herida quirúrgica obstétrica</li>
                  <li>Hemorragia obstétrica puerperal</li>
                  <li>Diagnóstico y tratamiento de placenta previa o desprendimiento prematuro de placenta normoinserta</li>
                  <li>Endometritis puerperal</li>
                  <li>Pelviperitonitis</li>
                  <li>Choque séptico puerperal</li>
                  <li>Reparación uterina</li>
                </p>
              </div>

					</div>
				</div>
			</div>
		</section>

		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	    <!-- <script type="text/javascript" src="public/js/owl.carousel.min"></script> -->

	</body>
</html>
