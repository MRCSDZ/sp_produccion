<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<meta charset="UTF-8">
	    <!-- Bootstrap -->
	    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
	    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->

		<section id="">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-lg-6">
							<h3 class="text-center">¿Que es?</h3>
							<p>La red de servicios es el conjunto de establecimientos de salud que se articulan para garantizar el proceso de atencion, desde su centro de salud hasta hospitales de segundo nivel o de especialidades.</p>
						</div>
						<div class="col-lg-6">
							<h3 class="text-center">¿Que Ofrece?</h3>
							<p>Te ofrece atención médica en los centros de salud, donde la cobertura del seguro popular es al 100% y continuidad de atención de los padecimientos a nivel de especialidad en los hospitales del estado.</p>
						</div>
						<div class="col-lg-12">
							<br>
							<br>
							<center>
	  							<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d628922.5314969071!2d-116.68271778596616!3d32.21955160342052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shospital+general+baja+california!5e0!3m2!1ses-419!2smx!4v1476912351146" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								<br>
								<br>
	  						</center>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <!-- <script type="text/javascript" src="public/js/owl.carousel.min"></script> -->
	    <script src="public/js/sticky.js"></script>

	</body>
</html>
