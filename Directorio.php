<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<meta charset="UTF-8">
	    <!-- Bootstrap -->
	    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
	    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->


		<section id="">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title">Directorio</h3>
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-6">
									<img src="public/images/directorio/FranciscoVega.png" alt="" class="center-block">
								</div>
								<div class="col-lg-6">
									<i class="glyphicon glyphicon-user"></i> Francisco Arturo Vega de La Madrid <br>
									<i class="glyphicon glyphicon-asterisk"></i> Gobernador del Estado de Baja California <br>
									<i class="glyphicon glyphicon-map-marker"></i> Edificio del Poder Ejecutivo, 3er. Piso, Calzada Independencia No. 994, Centro Civico, Mexicali, B.C. C.P. 21000 <br>
									<i class="glyphicon glyphicon-phone-alt"></i> 686-558-10-00 Ext 1302 <br>
									<i class="glyphicon glyphicon-envelope"></i> gobernador@baja.gob.mx <br>
								</div>
							</div>
			  		</div>
					</div>

					<div class="col-lg-6">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-5">
									<img src="public/images/directorio/GuillermoTrejo.png" alt="" class="center-block">
								</div>
								<div class="col-lg-7">
									<i class="glyphicon glyphicon-user"></i> Guillermo Trejo Dozal <br>
									<i class="glyphicon glyphicon-asterisk"></i> Secretario de Salud y Director General de ISESALUD del Estado de Baja California <br>
									<i class="glyphicon glyphicon-map-marker"></i> Palacio Federal Ave. Pioneros No. 1005, Centro Cívico, Mexicali, B.C. C.P. 21000 <br>
									<i class="glyphicon glyphicon-phone-alt"></i> 686 -558-58-17 Ext 4114, 4100  <br>
									<i class="glyphicon glyphicon-envelope"></i> gtrejod@baja.gob.mx <br>
								</div>
							</div>
			  		</div>
					</div>

					<div class="col-lg-6">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-5">
									<p class="text-center">
										<h1><i class="glyphicon glyphicon-user gi-5x"></i></h1>
									</p>
								</div>
								<div class="col-lg-7">
									<i class="glyphicon glyphicon-user"></i> César Alejandro Monráz Sustaita <br>
									<i class="glyphicon glyphicon-asterisk"></i> Director General del Régimen de Protección Social en Salud de Baja California <br>
									<i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P.21020  <br>
									<i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 70001, 70002, 70005  <br>
									<i class="glyphicon glyphicon-envelope"></i> alejandro.monraz@ seguropopularbc.gob.mx <br>
								</div>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>
									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Spíndola Rodríguez Mario César<br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Coordinador Ejecutivo Jurídico <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 78002 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i>  mario.spindola@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Ignacio López Cortés <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Coordinador Ejecutivo de Estrategias para la Organización y Enlace <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 70003 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> ignacio.cortes@ seguropopularbc.gob.mx<br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Gustavo Santos Hernández Valenzuela <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Director de Administración <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 70004 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> gustavo.hernandez@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> José Fernando Canizales Méndez <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Coordinador Ejecutivo de Gestión de Recursos<br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 76001 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i>  jose.canizales@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Iván Hernández Dávila  <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Jefe del Departamento de Recursos Materiales y Servicios Administrativos <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 78002 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> ivan.hernandez@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>
									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Patricia Del Bosque Aguilera  <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Jefe del Departamento de Recursos Financieros <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 78001 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> patricia.delosque@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Selso Alzate Fonseca  <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Jefe del Departamento de Recursos Humanos <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 71001 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> selso.alzate@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Marco Antonio De La Fuente Villarreal <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i>  Director de Gestión de Servicios de Salud <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 74111 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> marco.delafuente@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Héctor Francisco Ayala Valle <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Jefe del Departamento de Modelo y Garantía de Atención al Beneficiario <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Plateros No. 2003, Col. Burocrata, Mexicali, B.C. C.P. 21020 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 686-556-09-22 Ext 74016 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> hector.ayala@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i>  Reynaldo Bojórquez López  <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Director de Afiliación y Operación <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Dinamarca No. 9130, entre Guanajuato y Fresnillo, Col. Cacho, Tijuana, B.C. C.P. 22040 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 664-682-81-99 Ext 83003 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> reynaldo.bojorquez@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Manuel Antonio Delgado Cristerna <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Jefe del Departamento de Promoción y Afiliación <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Dinamarca No. 9130, entre Guanajuato y Fresnillo, Col. Cacho, Tijuana, B.C. C.P. 22040 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 664-682-81-99 Ext 84002 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> manuel.delgado@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>

					<div class="col-lg-3">
						<div class="panel panel-default" >
			    		<div class="panel-body">
								<div class="col-lg-12">
									<p class="text-center">
										<i class="glyphicon glyphicon-user gi-4x"></i>

									</p>
								</div>
							</div>
							<div class="panel-footer" >
								<p><i class="glyphicon glyphicon-user"></i> Javier Dávalos Venegas <br></p>
								<p><i class="glyphicon glyphicon-asterisk"></i> Jefe del Departamento de Administración de Base de Datos <br></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> Dinamarca No. 9130, entre Guanajuato y Fresnillo, Col. Cacho, Tijuana, B.C. C.P. 22040 <br></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> 664-682-81-99 Ext 84010 <br></p>
								<p><i class="glyphicon glyphicon-envelope"></i> javier.davalos@ seguropopularbc.gob.mx <br></p>
							</div>
			  		</div>
					</div>


























				</div>
			</div>
		</section>


		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <!-- <script type="text/javascript" src="public/js/owl.carousel.min"></script> -->
	    <script src="public/js/sticky.js"></script>

	</body>
</html>
