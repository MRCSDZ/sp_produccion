<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
      	<!-- ../Header -->

		<section id="modulo">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
  						<h3 class="title">Modulos de afiliacion</h3>
  						<center>
  							<!--<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d628922.5314969071!2d-116.68271778596616!3d32.21955160342052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shospital+general+baja+california!5e0!3m2!1ses-419!2smx!4v1476912351146" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
								<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1v6j5Rd_vtccTvyxgHU41hsM99qU" width="100%" height="480"></iframe>
							<br>
							<br>
  						</center>
  					</div>

					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">Encuentra tu modulo</div>
							<div class="panel-body">
			  					<ul class="nav nav-tabs">
			    					<li class="active"><a data-toggle="tab" href="#Mexicali">Mexicali</a></li>
			    					<li><a data-toggle="tab" href="#Rosarito">Rosarito</a></li>
			    					<li><a data-toggle="tab" href="#Tijuana">Tijuana</a></li>
			    					<li><a data-toggle="tab" href="#Tecate">Tecate</a></li>
									<li><a data-toggle="tab" href="#Ensenada">Ensenada</a></li>
			  					</ul>
			  					<div class="tab-content">
			    					<div id="Mexicali" class="tab-pane fade in active">
										<br>
										<br>
					      				<h3>Mexicali</h3>
										<p class="text-left">

											<b>Modulo:</b> Vicente guerrero<br>
											<b>Domicilio:</b> Av. Martin Carrera y General José Morán s/n, Col Felipe Ángeles<br>
											<b>Telefono:</b> (686) 554 6929<br>
											<b>Horario:</b> Lunes a Viernes de 07:00 a 15:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> Hospital Materno Infantil<br>
											<b>Domicilio:</b> Av. de la Claridad, Col. Plutarco Elías Calles<br>
											<b>Telefono:</b> (686) 841 3742<br>
											<b>Horario:</b> Lunes a Viernes de 07:00 a 15:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> C.S. Salinas de Gortari<br>
											<b>Domicilio:</b> Av. Del Rosario s/n entre Bahía Lucerna y Río Plata, Col. Salinas de Gortari <br>
											<b>Telefono:</b> (686) 844 2301<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> Guadalupe Victoria Km 43<br>
											<b>Domicilio:</b> Calle 10 y Av. 27 de enero, Cd. Guadalupe Victoria <br>
											<b>Telefono:</b> (658) 516 4011<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> C.S. San Felipe<br>
											<b>Domicilio:</b> Mar Bermejo entre Chetumal y Ensenada, Zona Centro<br>
											<b>Telefono:</b> (686) 577 0294<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>


											<b>Modulo:</b> Hospital General <br>
											<b>Domicilio:</b> Av. del Hospital s/n, Centro Cívico y Comercial<br>
											<b>Telefono:</b> (686) 557 4227<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> C.S. Puebla<br>
											<b>Domicilio:</b> Ignacio Zaragoza s/n, Ejido Puebla, entre Conalep y Del Álamo<br>
											<b>Telefono:</b> (686) 580 3327<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>




										</p>
			    					</div>
			    					<div id="Rosarito" class="tab-pane fade">
										<br>
										<br>
				      					<h3>Rosarito</h3>
										<p>
											<b>Modulo:</b> Playas de Rosarito<br>
											<b>Domicilio:</b> Plaza Palacio, Local 3, Calle José Haros Aguilar 2003<br>
											<b>Codigo Postal:</b> 22710<br>
											<b>Telefono:</b> (661) 613 7049<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>
										</p>
			   						</div>
			    					<div id="Tijuana" class="tab-pane fade">
										<br>
										<br>
							      		<h3>Tijuana</h3>
										<p>


											<b>Modulo:</b> Matamoros<br>
											<b>Domicilio:</b> Av. Mexicali 1986, Fracc. Las Fuentes<br>
											<b>Telefono:</b> (664) 650 9172<br>
											<b>Horario:</b> lunes a viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> Hospital General<br>
											<b>Domicilio:</b> Av. Centenario 10851, Zona Río<br>
											<b>Telefono:</b> (664) 634 6508<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> C.S Mesa de otay<br>
											<b>Domicilio:</b> Manuel m. carpio y salvador díaz mirón s/n, fraccionamiento nuevo tijuana modulo 4 <br>
											<b>Telefono:</b> (664) 623 4744<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> C.S. Cale 8<br>
											<b>Domicilio:</b> Av. Constitución 1641, entre 8va y 9na<br>
											<b>Telefono:</b> (664) 638 2264<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>
										</p>
			   						</div>

			    					<div id="Tecate" class="tab-pane fade">
										<br>
										<br>
								      	<h3>Tecate</h3>
										<p>
											<b>Modulo:</b> Tecate<br>
											<b>Domicilio:</b> quinta avenida no. 69 colonia benito juárez<br>
											<b>Telefono:</b> (665) 521 2130<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>
										</p>
								    </div>
									<div id="Ensenada" class="tab-pane fade">
										<br>
										<br>
										<h3>Ensenada</h3>
										<p>

											<b>Modulo:</b> C.S. Maneadero<br>
											<b>Domicilio:</b> Carretera Transpeninsular Km 20, Maneadero Parte Alta s/n<br>
											<b>Telefono:</b> (646) 154 0263<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> Hospital General <br>
											<b>Domicilio:</b> Av. Reforma y Ejército Nacional Km 111, Col. Carlos Pacheco<br>
											<b>Telefono:</b> (646) 173 5267<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> C.S. Ruiz y 14<br>
											<b>Domicilio:</b> Calle Ruiz y 14 1380, Zona Centro<br>
											<b>Telefono:</b> (646) 178 0829<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

											<b>Modulo:</b> Vicente Guerrero<br>
											<b>Domicilio:</b> Av. Vicente Guerrero 221, Esq. Calle Santo Domingo, Col. Vicente Guerrero <br>
											<b>Telefono:</b> (616) 166 3994<br>
											<b>Horario:</b> Lunes a Viernes de 08:00 a 16:00 hrs.<br>
											<br>
											<br>

										</p>
							  		</div>
							  	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<!-- ../Footer -->
		<?php include 'footer.php'; ?>
		<!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	</body>
</html>
