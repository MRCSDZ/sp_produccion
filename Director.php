<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8">
    <!-- Bootstrap -->
    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->


		<section id="">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title">Mensaje del Director</h3>
						<br>
              <div class="col-lg-6">
                <img src="public/images/director.jpg" width="90%" alt="" >
              </div>
              <div class="col-lg-6">
								<blockquote>
    							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    							<footer>César Alejandro Monráz Sustaita</footer>
  							</blockquote>
              </div>
					</div>
				</div>
			</div>
		</section>

		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->

	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	    <!-- <script type="text/javascript" src="public/js/owl.carousel.min"></script> -->

	</body>
</html>
