<div class="container-fluid" id="header">
	<div class="container">
		<div class="logos_mobile visible-xs">
			<div class="col-sm-4 header-img"><img src="public/images/logof.png" ></div>
			<div class="col-sm-4 header-img"><img src="public/images/log_bc.png" ></div>
			<div class="col-sm-4 header-img"><img src="public/images/logosp.jpg" ></div>
		</div>
		<div class="navbar navbar-default navbar-static-top">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="hidden-xs">
					<div class="col-lg-4 col-md-4 col-sm-4 header-img"><img src="public/images/logof.png" ></div>
					<div class="col-lg-4 col-md-4 col-sm-4 header-img"><img src="public/images/log_bc.png" ></div>
					<div class="col-lg-4 col-md-4 col-sm-4 header-img"><img src="public/images/logosp.jpg" ></div>
				</div>
			</div>
			<div class="navbar-collapse collapse ">
				<ul class="nav navbar-nav sticky">
					<li class="active"><a href="index.php">Inicio</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Seguro Popular <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="Directorio.php">Directorio</a></li>
							<li><a href="Director.php">Mensaje del Director</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="public/pdfs/organizacion/manual_update.pdf" target="_blank">Manual</a></li>
							<li><a href="public/pdfs/organizacion/encuesta.pdf" target="_blank">Encuesta</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="public/pdfs/organizacion/plantilla3.pdf" target="_blank">Organigrama</a></li>
							<li><a href="public/pdfs/organizacion/reglamento_interno.pdf" target="_blank">Reglamento Interno</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="public/pdfs/organizacion/decreto_repss.pdf" target="_blank">Decreto REPSS</a></li>
							<li><a href="public/pdfs/organizacion/manual_REPSS.pdf" target="_blank">Manual REPSS</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Afiliate <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="Afiliacion.php">Requisitos de Afiliacion </a></li>
							<li><a href="Modulos.php">Módulos de Afiliacion</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="FAQ.php">Preguntas Frecuentes</a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servicios de Salud <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="Gestores.php">¿Quien es tu gestor?</a></li>
							<li><a href="public/pdfs/ManualGestores.pdf" target="_blank">Manual del gestor</a></li>
							<li><a href="UbicaGestor.php">Ubica a tu gestor</a></li>

							<li role="separator" class="divider"></li>

							<li class="dropdown-submenu">
								<a class="test" href="#">Cobertura <span class="caret"></span></a>
								<ul class="dropdown-menu">
              		<li><a href="public/pdfs/Causes2016.pdf" target="_blank">CAUSES</a></li>
              		<li><a href="fpgc.php">FPGC</a></li>
									<li><a href="smsxxi.php">SMSXXI</a></li>
									<li><a href="EmbarazoSaludable.php">Embarazo Saludable</a></li>
            		</ul>
							</li>
							<li><a href="public/pdfs/Restricciones.pdf" target="_blank">Restricciones</a></li>
						</ul>
					</li>

					<li><a href="Red_de_Servicios.php">Red de Servicios</a></li>
					<li><a href="public/pdfs/Carta_derechos_Obligaciones.pdf" target="_blank">Carta de Derechos y Obligaciones</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sala de Prensa <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="blog/category/noticia/">Noticias </a></li>
							<li><a href="Videos.php">Videos</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="blog/category/comunicados/">Comunicados</a></li>
						</ul>
					</li>
					<li><a href="Poliza.php" id="poliza" >Consulta tu Poliza</a></li>

					<!--
					<li><a href="index.php">Imprime Tu Poliza (?)</a></li>
				  -->

				</ul>
			</div>
		</div>
	</div>
</div>
