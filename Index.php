<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular</title>
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<meta charset="UTF-8">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	    <!-- Bootstrap -->
	    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
	    <link href="public/css/main.css" rel="stylesheet">
			<!--Meta Default-->
			<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
			<meta name="thumbnail" content="public/images/icono.png" />
			<meta name="description" content="Seguro Popular, información sobre el Seguro Popular de México, donde encontrarás desde los requisitos para el seguro popular, hasta  las enfermedades que cubre.">
			<meta name="keywords" content="seguro popular, seguro popular requisitos, requisitos para el seguro popular, seguro popular mexicali, seguro popular tijuana, seguro popular ensenada, seguro popular tecate, seguro popular rosarito" />

			<!--Meta Social-->
			<meta property="og:url" content="seguropopularbc.gob.mx" />
			<meta property="og:title" content="Pagina del Seguro Popular de Baja California"/>
			<meta property="og:image" content="public/images/icono.png"/>
			<meta property="og:site_name" content="Seguro popular de Baja California"/>
			<meta property="og:description" content="Seguro Popular, información sobre el Seguro Popular de México, donde encontrarás desde los requisitos para el seguro popular, hasta  las enfermedades que cubre."/>



	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
        <!-- ../Header -->


        <!-- ../Index.html -->
        <section id="index">
        	<div class="container">
        		<div align="center" class="embed-responsive embed-responsive-16by9 section_video">
				    <video autoplay loop muted class="embed-responsive-item" controls>
				        <source src=http://www.seguropopularbc.gob.mx/fotos/gtsp.mp4 type=video/mp4>
				    </video>
				</div>
				<div class="section_banner">
					<img src="public/images/semana2.jpg" alt="" width="100%"  class="center-block" />
				</div>
        	</div>
					<!--Carrousel-->
					<!--Carrousel-->
        	<div class="container-fluid section_carousel">
		        <div class="container">
		        	<div id="owl-demo" class="owl-carousel owl-theme">

								<div class="item" id="noticia0">
									<div class="row">
										<!-- <div class="col-lg-12">
											<h1></h1>
										</div> -->
										<div class="col-lg-6 col-xs-12">
											<img src="" alt="">
								    </div>
										<div class="col-lg-6 col-xs-12">
											<h1></h1>
											<div class="fecha">
											</div>
											<div class="contenido">
											</div>
								    </div>
									</div>
								</div>

								<div class="item" id="noticia1">
									<div class="row">
										<!-- <div class="col-lg-12">
											<h1></h1>
										</div> -->
										<div class="col-lg-6 col-xs-12">
											<img src="" alt="">
								    </div>
										<div class="col-lg-6 col-xs-12">
											<h1></h1>
											<div class="fecha">
											</div>
											<div class="contenido">
											</div>
								    </div>
									</div>
								</div>

								<div class="item" id="noticia2">
									<div class="row">
										<!-- <div class="col-lg-12">
											<h1></h1>
										</div> -->
										<div class="col-lg-6 col-xs-12">
											<img src="" alt="">
								    </div>
										<div class="col-lg-6 col-xs-12">
											<h1></h1>
											<div class="fecha">
											</div>
											<div class="contenido">
											</div>
								    </div>
									</div>
								</div>

								<div class="item" id="noticia3">
									<div class="row">
										<!-- <div class="col-lg-12">
											<h1></h1>
										</div> -->
										<div class="col-lg-6 col-xs-12">
											<img src="" alt="">
								    </div>
										<div class="col-lg-6 col-xs-12">
											<h1></h1>
											<div class="fecha">
											</div>
											<div class="contenido">
											</div>
								    </div>
									</div>
								</div>

								<div class="item" id="noticia4">
									<div class="row">
										<!-- <div class="col-lg-12">
											<h1></h1>
										</div> -->
										<div class="col-lg-6 col-xs-12">
											<img src="" alt="">
								    </div>
										<div class="col-lg-6 col-xs-12">
											<h1></h1>
											<div class="fecha">
											</div>
											<div class="contenido">
											</div>
								    </div>
									</div>
								</div>

							</div>
						</div>
        	</div>
					<!--Fin Carrousel-->

					<!--Fin Carrousel-->

    		<div class="container section_info">
				<div class="row">
					<div class="col-lg-4 col-sm-4">
						<div class="panel panel-danger" >
							<div class="panel-heading" id="mvp">Misión</div>
			    			<div class="panel-body">Garantizar los servicios de salud a la población de Baja California sin seguridad Social, a través del aseguramiento al programa Seguro Popular, otorgando protección financiera para la atención de la salud</div>
			  			</div>
					</div>
					<div class="col-lg-4 col-sm-4">
						<div class="panel panel-danger" >
							<div class="panel-heading" id="mvp">Visión </div>
			    			<div class="panel-body">Cobertura Universal en el estado de Baja California en el aseguramiento de los servicios de salud y la tutela de los derechos de los afiliados, otorgando servicios de calidad y calidez.</div>
			  			</div>
					</div>

						<div class="col-lg-4 col-sm-4">
						<div class="panel panel-danger" >
							<div class="panel-heading" id="mvp">Politica de calidad</div>
			    		<div class="panel-body">
								 Nuestro compromiso es mantener la cobertura universal para tutelar la salud de los afiliados al Sistema de Protección Social en Salud de Baja California, mediante un esquema de financiamiento de servicios de salud ofrecidos con calidez en un ambiente de una mejora continua.
							</div>
			  			</div>
					</div>
				</div>
				<div class="row downloads">
					<div class="col-lg-3 col-sm-6">
			  			<div class="panel panel-default" >
			  				<div class="panel-heading" id="causes">CAUSES 2016 </div>
						  	<div class="panel-body">
								<p class="text-center">
									<i class="glyphicon glyphicon-heart gi-4x"></i><br><br>
    						    	Catálogo Universal de Servicios de Salud.
								</p>
							</div>
							<a href="public/pdfs/Causes2016.pdf" target="_blank"><div class="panel-footer" id="causes">Descargar</div></a>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="panel panel-default" >
						 	<div class="panel-heading"id="anexo">ANEXO 2016</div>
			    			<div class="panel-body">
								<p class="text-center">
									<i class="glyphicon glyphicon-save-file gi-4x"></i><br><br>
									Catalogo de insumos de servicios de salud
								</p>
							</div>
							<a href="public/pdfs/Anexos2016.pdf" target="_blank"><div class="panel-footer"id="anexo">Descargar</div></a>
			  			</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div id="afiliacion" class="panel panel-default" >
							<div class="panel-heading">Requisitos de afiliacion</div>
			    			<div class="panel-body">
								<p class="text-center">
									<i class="glyphicon glyphicon-question-sign gi-4x"></i><br><br>
					    			Lista de requisitos para seguro popular.
								</p>
							</div>
							<div class="panel-footer">
								<a href="http://www-2.baja.gob.mx/intranet/Sitrase.nsf/1d64bffad0d424e988257909006072f9/bdf77d616d42ad40882578f0005ecbf4?OpenDocument">Acceder</a>
							</div>
			  			</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="panel panel-default" >
							<div class="panel-heading" id="centrosalud">Red prestadora de servicios</div>
    			    		<div class="panel-body">
								<p class="text-center">
									<i class="glyphicon glyphicon-globe gi-4x"></i><br><br>
									Conoce tu centro de salud <br><br>
								</p>
							</div>
							<a href="#"><div class="panel-footer" id="centrosalud" >Acceder</div></a>
			  			</div>
					</div>
				</div>

				<div class="col-lg-12 atencion_telefonica">
					<img src="public/images/AtencionTelefonica.jpg" alt="Atencion telefonica" class="center-block"/>
				</div>
    		</div>
        </section>
		<!-- ../Index.html -->



		<!-- ../Footer -->
	    <?php include 'footer.php'; ?>
	    <!-- ../Footer -->


			<script type="text/javascript" src="public/js/noticias.js"></script>
	    <script type="text/javascript" src="public/js/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	    <script type="text/javascript" src="public/js/owl.carousel.min.js"></script>

		<script type="text/javascript">
	    	$(document).ready(function() {
			 	$("#owl-demo").owlCarousel({
				    slideSpeed : 300,
				    paginationSpeed : 400,
				    singleItem:true,
				    navigation:true,
				    pagination:true,
				    autoPlay:5000,
				    stopOnHover:true,
				    navigationText: ''
				});
			});


	    </script>
			
	</body>
</html>
