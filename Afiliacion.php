<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<meta charset="UTF-8">
	    <!-- Bootstrap -->
	    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
	    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>

		<style>
			.button {
				display: block;
				max-width: 350px;
				margin: 40px auto 0;
				padding: 15px 25px;
				font-size: 24px;
				text-align: center;
				cursor: pointer;
				outline: none;
				color: #fff;
				background-color: #c32f44;
				border: none;
				border-radius: 5px;
				box-shadow: 0 9px #999;
			}
			.button:hover {
				background-color: #9a2536;
				color:#fff;
			}
			.button:active {
				background-color: #9a2536;
				box-shadow: 0 5px #666;
				transform: translateY(4px);
			}

			.info img{
				max-width: 200px;
			}
		</style>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
      	<!-- ../Header -->



		<section id="">
			<div class="container">
				<div class="row">
					<h3 class="title">Afiliación</h3>
					<div class="col-lg-12">
						<center>
							<img src="public/images/afiliate.png" width="65%" alt="" />
						</center>
					</div>

					<div class="col-lg-4 info">
						<br>
						<br>

						<center>
							<h3>¿Quién se Puede Afiliar?</h3>
			        		<img align="center" src="http://seguropopular.saludsonora.gob.mx/img/dummies/afiliar.png">
			        	</center>
						<p align="justify">Cualquier persona que no sea derechohabiente de la seguridad social en salud, es decir aquellos que trabajan por cuenta propia y no estén afiliados a alguna institución de salud. El nucleo familiar:</p>
		                 <ul>
		                  	<li> El padre y/o la madre.</li>
		                    <li> Los hijos menores de edad.</li>
		                    <li> Los menores de edad que formen parte del hogar y tengan parentesco de consanguinidad.</li>
		                    <li> Los hijos solteros de hasta 25 años que prueben ser estudiantes.</li>
		                    <li> Los hijos discapacitados dependientes.</li>
		                    <li> Los ascendientes directos mayores de 64 años, que sean dependientes económicos y vivan en el mismo hogar.</li>
		                    <li>Los menores de 18 años o discapacitados que aún no teniendo parentesco de consanguinidad con el jefe de familia, habiten en la misma vivienda y dependan económicamente de él/ella.</li>
		                    <li>Las personas mayores de 18 años de manera individual.</li>
		                    <li>Las personas que vivan solas, mayores de edad que vivan con sus padres o familiares.</li>
		                    <li>Mujeres embarazadas o con hijos menores de edad.</li>
						</ul>
					</div>

					<div class="col-lg-4 info">
						<br>
						<br>
						<center>
							<h3>¿Qué Documentos Ocupas?</h3>
			        		<img align="center" src="http://seguropopular.saludsonora.gob.mx/img/dummies/docs.png">
			        	</center>
						<p align="justify">Presentar copia de los siguientes documentos:</p>
		                <ul>
		               		<li> Comprobante de domicilio (no mayor a 3 meses).</li>
		               		<li> CURP o acta de nacimiento de todos los integrantes de la familia.</li>
		               		<li> En el caso de recién nacidos, certificado de nacimiento.</li>
		               		<li> Identificación oficial vigente.</li>
		               		<li> De ser el caso, comprobante de que eres beneficiario de algún programa de apoyo o subsidio del Gobierno Federal o comprobante que te acredite como parte de alguna colectividad.</li>
		               		<li> Comprobante de estudios de los hijos o representados dependientes del titular de hasta 25 años de edad, de ser el caso.</li>
		               		<li>No contar con otra seguridad social.</li>
		               	</ul>
					</div>

					<div class="col-lg-4 info">
						<br>
						<br>
						<center><h3>Reafiliación</h3>
		        		<img align="center" src="http://seguropopular.saludsonora.gob.mx/img/dummies/reafiliacion.png"></center>
						<p align="justify"> Reafiliarte al Seguro Popular es muy sencillo, sólo necesitas:</p>
		               	<ul>
		               		<li> Asistir al Módulo de Afiliación que te corresponda.</li>
		               		<li> Presentar tu póliza vencida en original.</li>
		               		<li> Entregar copia de los siguientes documentos:
		               			<ul>
		               				<li> Comprobante de domicilio (no mayor a 3 meses).</li>
		               				<li> No contar con otra seguridad social.</li>
		               				<li> En el caso de los recién nacidos: certificado de nacimiento.</li>
		               				<li> Identificación oficial vigente.</li>
		               			</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<a class="button" href="public/pdfs/Afiliacion.pdf" target="_blank"> Descargar Afiliación</a>
				</div>

			</div>
		</section>

		<!-- ../Footer -->
		<?php include 'footer.php'; ?>
		<!-- ../Footer -->


	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	</body>
</html>
