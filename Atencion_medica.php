<!DOCTYPE html>
<html>
	<head>
		<title>Seguro Popular -</title>
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<meta charset="UTF-8">
	    <!-- Bootstrap -->
	    <link href="public/css/bootstrap-3.3.7.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
	    <link href="public/css/main.css" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="public/images/icono.png"/>
		<style>
			.info img{
				max-width: 200px;
			}
		</style>
	</head>
	<body>
		<!-- ../Header -->
		<?php include 'header.php'; ?>
      	<!-- ../Header -->



		<section id="">
			<div class="container">
				<div class="row">
					<h3 class="title">Atención Médica</h3>
					<div class="col-lg-4 info">
						<center>
							<h3>¿Qué Te Ofrece?</h3>
		        			<img align="center" src="http://seguropopular.saludsonora.gob.mx/img/dummies/medica1.png">
		        		</center>
						<p align="justify"> Seguro Popular se ha creado como un sistema de salud que responda a las aspiraciones de los mexicanos. Para cumplir con esa premisa, se orienta en atender cinco aspectos fundamentales:</p>
		                <ul>
		                  	<li> Mejorar las condiciones de salud de los mexicanos.</li>
		                    <li> Abatir las desigualdades en salud.</li>
		                    <li> Garantizar un trato adecuado en los servicios públicos y privados en salud.</li>
		                    <li> Asegurar la justicia en el financiamiento en materia de salud.</li>
		                    <li> Fortalecer el Sistema Nacional de Salud, en particular sus instituciones públicas.</li>
						</ul>
					</div>
					<div class="col-lg-4 info">
						<center>
							<h3>¿Para Quién Es?</h3>
		        			<img align="center" src="http://seguropopular.saludsonora.gob.mx/img/dummies/medica2.png">
		        		</center>
						<p align="justify">Todas las personas que no tienen un trabajo bajo un régimen asalariado como un empleado, es decir que los trabajadores que no reciben un salario fijo, como la mayoria de los productores del campo, las personas que laboran por cuenta propia en el ámbito rural o en las cuidades, los pequeños comerciantes, los emprendedores independientes, los profecionistas en el ejercicio libre, los que cobran por honorarios o por comisión, las amas de casa, y los estudiantes.</p>
		               	<p align="justify">La falta de protección financiera reside en mas de 48 millones de mexicanos que no tienen una cobertura de servicios de salud. A pesar de los avances de la seguridad social, más de la mitad del gasto de salud en México procede directamente del bolsillo de las personas, quienes deben pagar por su atencion en el momento mismo de usar los servicios. Esto deja millones de familiasante la disyuntiva de empobrecerse o ver a sus seres queridos sufrir alguna enfermedad y dolor por falta de recursos económicos.</p>
					</div>
					<div class="col-lg-4 info">
						<center>
							<h3>CAUSES 2016</h3>
		        			<img align="center" src="http://seguropopular.saludsonora.gob.mx/img/dummies/causes.png">
		        		</center>
						<p align="justify">Catálogo Universal de Servicos de Salud (CAUSES) contiene la relación de acciones médicas que tú y tu familia recibirán sin tener que pagar ninguna cuota en el momento de recibir la atencion medica. </p>
		               <a href="public/pdfs/Causes2016.pdf" download="Causes2016">Conoce el CAUSES 2016</a>
					</div>
				</div>
		     </div>
		</section>

		<!-- ../Footer -->
		<?php include 'footer.php'; ?>
		<!-- ../Footer -->


	    <!-- <script type="text/javascript" src="puclic/js/jquery-3.1.1.min.js"></script> -->
	    <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script type="text/javascript" src="public/js/bootstrap-3.3.7.min.js"></script>
	    <script src="public/js/sticky.js"></script>
	</body>
</html>
